# Algorithm to find the best day to buy and sell stocks in a list
# Exercise 1 in IDATT2101
# Created by Edvard Schøyen

import time
import random


def StockCalculator(prices):
    buyDay, sellDay, biggestSale, profit = 0, 0, 0, 0
    for idx, iPrice in enumerate(prices):
        value = 0
        for idx2, jPrice in enumerate(prices[idx + 1:]):
            value += jPrice
            salesPrice = value - iPrice
            if salesPrice > biggestSale:
                biggestSale = salesPrice
                profit = value
                buyDay = idx
                sellDay = idx + idx2 + 1

    return buyDay, sellDay, profit


prices = [-1, 3, -9, 2, 2, -1, 2, -1, -5]
prices1 = [random.randint(-10, 10) for _ in range(1000)]
prices2 = [random.randint(-10, 10) for _ in range(5000)]
prices3 = [random.randint(-10, 10) for _ in range(10000)]
prices4 = [random.randint(-10, 10) for _ in range(50000)]
prices5 = [random.randint(-10, 10) for _ in range(100000)]

buyDay, sellDay, profit = StockCalculator(prices)

print("")
print("Stock bought on day", buyDay + 1)
print("Stock sold on day", sellDay + 1)
print("Profit", profit)

start0 = time.time()
StockCalculator(prices1)
end0 = time.time()

start1 = time.time()
StockCalculator(prices2)
end1 = time.time()

start2 = time.time()
StockCalculator(prices3)
end2 = time.time()

start3 = time.time()
StockCalculator(prices4)
end3 = time.time()

start4 = time.time()
StockCalculator(prices5)
end4 = time.time()

print("Time of array with 1000 elements", end0 - start0, " seconds")

print("Time of array with 5000 elements", end1 - start1, " seconds")

print("Time of array with 10 000 elements", end2 - start2, " seconds")

print("Time of array with 50 000 elements", end3 - start3, " seconds")

print("Time of array with 100 000 elements", end4 - start4, " seconds")
