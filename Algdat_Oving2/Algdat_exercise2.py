# Exercise 2 in IDATT2101
# Created by Edvard Schøyen
import math
import sys
import time


def pow(x, n):
    if n == 0:
        return 1
    else:
        return x * pow(x, n - 1)


def pow2(x, n):
    if n == 0:
        return 1
    if n % 1 == 1:
        return x * pow2(x * x, (n - 1) / 2)
    else:
        return pow2(x * x, n / 2)


def mathpow(x, n):
    return math.pow(x, n)


# To increase the standard python recursion limit
limit = 5000
sys.setrecursionlimit(limit)

print("\nRecursion limit set to", sys.getrecursionlimit())
x = 1.001
n = 1000

print("x = ", x, "n = ", n, "\n")
start = time.time()
exp = pow(x, n)
end = time.time()

time1 = end - start

print(x, "^", n, "=", exp)
print(("%.10f" % time1).rstrip('0').rstrip('.'), "seconds with method 1")
print("")

start_2 = time.time()
exp_2 = pow2(x, n)
end_2 = time.time()

time2 = end_2 - start_2

print(x, "^", n, "=", exp_2)
print(("%.10f" % time2).rstrip('0').rstrip('.'), "seconds with method 2")
print("")

start_3 = time.time()
power = mathpow(x, n)
end_3 = time.time()

time3 = end_3 - start_3

print("math.pow(", x, ",", n, ") =", power, "")
print(("%.10f" % time3).rstrip('0').rstrip('.'), "seconds with math.pow")
