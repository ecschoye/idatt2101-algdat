# Exercise 3 in IDATT2101
# Created by Edvard Schøyen

import random
import time
import copy

# Quicksort from "Algoritmer og datastrukter"
def quicksort(list, left, right):
    if right - left > 2:
        divpos = split(list, left, right)
        quicksort(list, left, divpos - 1)
        quicksort(list, divpos + 1, right)
    else:
        median3sort(list, left, right)


def split(list, left, right):
    m = median3sort(list, left, right)
    dv = list[m]
    swap(list, m, right - 1)
    idxleft = left
    idxright = right - 1

    for index in range(idxleft, idxright):
        idxright -= 1
        idxleft += 1
        while list[++idxleft] < dv:
            idxleft += 1
        while list[idxright] > dv:
            idxright -= 1
        if idxleft >= idxright:
            break
        swap(list, idxleft, idxright)
    swap(list, idxleft, right - 1)
    return idxleft


def swap(list, i, j):
    list[i], list[j] = list[j], list[i]


def median3sort(list, left, right):
    m = (left + right) // 2
    if list[left] > list[m]:
        swap(list, left, m)
    if list[m] > list[right]:
        swap(list, m, right)
        if list[left] > list[m]:
            swap(list, left, m)
    return m


def partitionQS(list, low, high):
    pivot = list[low]
    i, j = low, high
    if list[i] > list[j]:
        list[i], list[j] = list[j], list[i]


# Python3 program to implement
# dual pivot QuickSort
# This code is contributed by Gourish Sadhu


def dualPivotQuickSort(arr, low, high):
    if low < high:
        # lp means left pivot and rp
        # means right pivot
        lp, rp = partition(arr, low, high)

        dualPivotQuickSort(arr, low, lp - 1)
        dualPivotQuickSort(arr, lp + 1, rp - 1)
        dualPivotQuickSort(arr, rp + 1, high)


def partition(arr, low, high):
    # swaps the number in arr[low] position with the number inn arr[low + (high - low) // 3] position
    # swaps the number in arr[high] position with the number inn arr[high - (high - low) // 3] position
    # this is to avoid O(n**2) time complexity in case of sorting an already sorted array
    arrhigh = low + (high - low) // 3
    arrlow = high - (high - low) // 3

    if high > low:
        swap(arr, arrhigh, high)
        swap(arr, arrlow, low)
    else:
        swap(arr, arrlow, high)
        swap(arr, arrhigh, low)

    if arr[low] > arr[high]:
        arr[low], arr[high] = arr[high], arr[low]

    # p is the left pivot, and q is the right pivot.
    j = k = low + 1
    g, p, q = high - 1, arr[low], arr[high]

    while k <= g:

        # If elements are less than the left pivot
        if arr[k] < p:
            arr[k], arr[j] = arr[j], arr[k]
            j += 1

        # If elements are greater than or equal
        # to the right pivot
        elif arr[k] >= q:
            while arr[g] > q and k < g:
                g -= 1

            arr[k], arr[g] = arr[g], arr[k]
            g -= 1

            if arr[k] < p:
                arr[k], arr[j] = arr[j], arr[k]
                j += 1

        k += 1

    j -= 1
    g += 1

    # Bring pivots to their appropriate positions.
    arr[low], arr[j] = arr[j], arr[low]
    arr[high], arr[g] = arr[g], arr[high]

    # Returning the indices of the pivots
    return j, g

def checkifsorted(list):
    for i in range(len(list) - 1):
        if list[i] > list[i + 1]:
            return False
    return True

def createList(size):
    list = []
    for i in range(size):
        list.append(random.randint(0, 100000))
    return list

def createSortedList(size):
    list = []
    for i in range(size):
        list.append(i)
    return list

def createListWithDuplicates(size):
    list = []
    for i in range(size):
        list.append(random.randint(0, 5))
    return list

def createListWithTwoNumbers(size):
    list = []
    for i in range(size):
        if i % 2 == 0:
            list.append(1)
        else:
            list.append(2)
    return list


# n amount of elements in list
n = 1000000

# list 1 with random numbers
print("\nQuicksort list 1 (random numbers)")
quicksortlist = createList(n)
dualquicksortlist = copy.deepcopy(quicksortlist)
sumbefore = sum(quicksortlist)
print("Is list sorted:", checkifsorted(quicksortlist))
print("Sum of list 1:", sum(quicksortlist))
start = time.time()
quicksort(quicksortlist, 0, len(quicksortlist) - 1)
end = time.time()
sumafter = sum(quicksortlist)
print("Is list sorted:", checkifsorted(quicksortlist))
print("Sum of list 1:", sum(quicksortlist))
print("Sum is the same:", sumbefore == sumafter)
print("Time to sort:", end - start)
print("\nDual pivot quicksort list 1 (random numbers)")
sumbefore = sum(dualquicksortlist)
print("Is list sorted:", checkifsorted(dualquicksortlist))
print("Sum of list 1:", sum(dualquicksortlist))
start = time.time()
dualPivotQuickSort(dualquicksortlist, 0, len(dualquicksortlist) - 1)
end = time.time()
sumafter = sum(dualquicksortlist)
print("Is list sorted:", checkifsorted(dualquicksortlist))
print("Sum of list 1:", sum(dualquicksortlist))
print("Sum is the same:", sumbefore == sumafter)
print("Time to sort:", end - start)


# list 2 with already sorted list
print("\nQuicksort list 2 (already sorted)")
quicksortlist = createSortedList(n)
dualquicksortlist = copy.deepcopy(quicksortlist)
sumbefore = sum(quicksortlist)
print("Is list sorted:", checkifsorted(quicksortlist))
print("Sum of list 2:", sum(quicksortlist))
start = time.time()
quicksort(quicksortlist, 0, len(quicksortlist) - 1)
end = time.time()
sumafter = sum(quicksortlist)
print("Is list sorted:", checkifsorted(quicksortlist))
print("Sum of list 2:", sum(quicksortlist))
print("Sum is the same:", sumbefore == sumafter)
print("Time to sort:", end - start)
print("\nDual pivot quicksort list 2 (already sorted)")
sumbefore = sum(dualquicksortlist)
print("Is list sorted:", checkifsorted(dualquicksortlist))
print("Sum of list 2:", sum(dualquicksortlist))
start = time.time()
dualPivotQuickSort(dualquicksortlist, 0, len(dualquicksortlist) - 1)
end = time.time()
sumafter = sum(dualquicksortlist)
print("Is list sorted:", checkifsorted(dualquicksortlist))
print("Sum of list 2:", sum(dualquicksortlist))
print("Sum is the same:", sumbefore == sumafter)
print("Time to sort:", end - start)

# list 3 quicksort
print("\nQuicksort list 3 (duplicates)")
quicksortlist = createListWithDuplicates(n)
dualquicksortlist = copy.deepcopy(quicksortlist)
sumbefore = sum(quicksortlist)
print("Is list sorted:", checkifsorted(quicksortlist))
print("Sum of list 3:", sum(quicksortlist))
start = time.time()
quicksort(quicksortlist, 0, len(quicksortlist) - 1)
end = time.time()
sumafter = sum(quicksortlist)
print("Is list sorted:", checkifsorted(quicksortlist))
print("Sum of list 3", sum(quicksortlist))
print("Sum is the same:", sumbefore == sumafter)
print("Time to sort", end - start)
print("\nDual pivot list 3 (duplicates)")
sumbefore = sum(dualquicksortlist)
print("Is list sorted:", checkifsorted(dualquicksortlist))
print("Sum of list 3:", sum(dualquicksortlist))
start = time.time()
dualPivotQuickSort(dualquicksortlist, 0, len(dualquicksortlist) - 1)
end = time.time()
sumafter = sum(dualquicksortlist)
print("Is list sorted:", checkifsorted(dualquicksortlist))
print("Sum of list 3", sum(dualquicksortlist))
print("Sum is the same:", sumbefore == sumafter)
print("Time to sort", end - start)
