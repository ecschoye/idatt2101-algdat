# Exercise 4 in IDATT2101
# Created by Edvard Schøyen

class Node:
    def __init__(self, data):
        self.data = data
        # reference to next node in DLL
        self.next = None
        # reference to previous node in DLL
        self.prev = None

    def findElement(self):
        return self.data

    def findNext(self):
        return self.next

    def findPrev(self):
        return self.prev


class DoublyLinkedList:
    def __init__(self):
        self.head = None

    def append(self, data):
        new_node = Node(data)
        if self.head is None:
            new_node.prev = None
            self.head = new_node
        else:
            cur = self.head
            while cur.next:
                cur = cur.next
            cur.next = new_node
            new_node.prev = cur
            new_node.next = None

    def prepend(self, data):
        new_node = Node(data)
        if self.head is None:
            new_node.prev = None
            self.head = new_node
        else:
            self.head.prev = new_node
            new_node.next = self.head
            self.head = new_node
            new_node.prev = None

    def delete(self, data):
        if self.head is None or data is None:
            return
        if self.head == data:
            self.head = data.next

        if data.next is not None:
            data.next.prev = data.prev

        if data.prev is not None:
            data.prev.next = data.next


    def removeZero(self):
        while self.head.data == 0:
            self.delete(self.head)

    def reverse(self):
        temp = None
        cur = self.head

        while cur is not None:
            temp = cur.prev
            cur.prev = cur.next
            cur.next = temp
            cur = cur.prev

        if temp is not None:
            self.head = temp.prev

    def getLength(self, list):
        size = 0
        while list:
            list = list.next
            size = size + 1
        return size

    def addTwoLists(self, first, second):
        third = DoublyLinkedList()
        carry = 0
        while first or second or carry:
            value1 = first.data if first else 0
            value2 = second.data if second else 0

            val = value1 + value2 + carry
            carry = val // 10
            val = val % 10
            third.append(val)

            first = first.next if first else None
            second = second.next if second else None

        third.reverse()
        print("Sum with addition")

        return third.printList()

    def subtractHelper(self, first, second):
        copyFirst = first
        copySecond = second
        sumFirst, sumSecond, power = 0, 0, 0
        while copyFirst or copySecond:
            value1 = copyFirst.data if copyFirst else 0
            value2 = copySecond.data if copySecond else 0
            sumFirst = sumFirst + (value1 * 10 ** power)
            sumSecond = sumSecond + (value2 * 10 ** power)
            copyFirst = copyFirst.next if copyFirst else None
            copySecond = copySecond.next if copySecond else None
            power = power + 1

        if sumFirst > sumSecond:
            return first, second
        else:
            return second, first


    def subtractTwoLists(self, first, second):
        result = DoublyLinkedList()
        first, second = DoublyLinkedList().subtractHelper(first, second)
        isValZero = False
        borrowed = False
        while first or second:
            value1 = first.data if first else 0
            value2 = second.data if second else 0
            if borrowed is True:
                value1 = value1 - 1
                borrowed = False

            if value1 < value2:
                borrowed = True
                value1 = value1 + 10

            val = value1 - value2

            result.append(val)
            first = first.next if first else None
            second = second.next if second else None
        result.reverse()
        result.removeZero()
        print("Sum with subtraction")
        return result.printList()

    def printList(self):
        current = self.head
        list = []
        while current:
            # print(current.data)
            list.append(current.data)
            current = current.next
        print(*list, sep="")


def addToNodeList(numbers):
    dllist = DoublyLinkedList()
    numlist = [int(x) for x in str(numbers)]
    for i in numlist:
        dllist.append(i)
    dllist.reverse()
    return dllist


if __name__ == "__main__":
    print("Addition: 1\nSubtraction: 2")
    chooseOperation = input("Enter the number of the operation you'd like:")
    inputnumbers = input("Enter first number ")
    dllist1 = addToNodeList(inputnumbers)
    inputnumbers = input("Enter second number ")
    dllist2 = addToNodeList(inputnumbers)
    answer = DoublyLinkedList()
    if chooseOperation == "1":
        answer.addTwoLists(dllist1.head, dllist2.head)
    elif chooseOperation == "2":
        answer.subtractTwoLists(dllist1.head, dllist2.head)
    else:
        print("Choose a valid operation")