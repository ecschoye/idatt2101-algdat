import java.io.*;
import java.util.LinkedList;

/**
 * Exercise 5a
 * Author: Edvard Schøyen
 */
public class HashTabell {
    private final LinkedList[] table;
    private final int tableSize;
    private int collisions;
    private int people;
    private int hash;

    public HashTabell(int size) {
        this.tableSize = size;
        this.table = new LinkedList[size];
        for (int i = 0; i < size; i++) {
            table[i] = new LinkedList();
        }
    }

    public void insert(String string) {
        if (isPowerOfTwo(tableSize)) hash = multiHash(string);
        else hash = divHash(string);
        if (!table[hash].isEmpty()) {
            collisions++;
        }
        people++;
        table[hash].add(string);
    }


    public String search(String string) {
        if (isPowerOfTwo(tableSize)) hash = multiHash(string);
        else hash = divHash(string);
        return table[hash].contains(string) ? containsPrint(true, string) : containsPrint(false, string);
    }

    public String containsPrint(boolean condition, String s) {
        return condition ? "Yes, it does contain: " + s : "No, it does not contain: " + s;
    }

    public void print() {
        for (int i = 0; i < tableSize; i++) {
            LinkedList entry = table[i];
            System.out.println("BucketIndex: " + i);
            for (Object o : entry) {
                System.out.print(o + "<---");
            }
            System.out.println("\n");
        }
    }

    public int getAmountOfCollissions() {
        return collisions;
    }

    public void getList() {
        System.out.println("List of all people in the hash table:");
        for (int i = 0; i < tableSize; i++) {
            LinkedList entry = table[i];
            for (Object o : entry) {
                System.out.println(o);
            }
        }
    }

    public double getAmountOfElements() {
        return people;
    }

    public double getLoadFactor() {
        return getAmountOfElements()/(double)tableSize;
    }

    public double getAvgCollisions() {
        return getAmountOfCollissions()/ getAmountOfElements();
    }

    private  int divHash(String string) {
        hash = 0;
        for (int i = 0; i < string.length(); i++) {
            hash += string.charAt(i) * 7 * (i + 1);
        }
        return hash % tableSize;
    }

    private int multiHash(String string) {
        hash = 0;
        for (int i = 0; i < string.length() ; i++) {
            hash += string.charAt(i);
        }
        return (int)(hash*1327127885L) >>> (32 - getPowerOfTwo(tableSize)) & (tableSize -1);
    }

    private boolean isPowerOfTwo(int n) {
        double m = n;
        if (m == 1) {
            return false;
        }
        while (m != 1) {
            m /= 2;
            if(m%2 != 0 && m != 1) return false;
        }
        return true;
    }

    private int getPowerOfTwo(int n) {
        if (n == 1) {
            return 0;
        }
        int pow = 0;
        while (n != 1) {
            n /= 2;
            pow++;
        }
        return pow;
    }



    public static void main(String[] args) throws FileNotFoundException {
        HashTabell hashTabell = new HashTabell(128);
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("navn.txt")));
            String text;
            while ((text = bufferedReader.readLine()) != null) {
                hashTabell.insert(text);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        hashTabell.print();
        System.out.println("Total amount of collisions: " + hashTabell.getAmountOfCollissions());
        System.out.println("Load factor " + hashTabell.getLoadFactor());
        System.out.println("Avg collisions " + hashTabell.getAvgCollisions());
        String name = "Edvard Christian Berg Schøyen";
        System.out.println("Does the table include: " + name + "?\n" + hashTabell.search(name) + "\n");
    }

}
