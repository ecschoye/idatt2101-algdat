import java.util.HashMap;

/**
 * Exercise 5b
 * Author: Edvard Schøyen
 */
public class HashTabell2 {
    private final int tableSize;
    private int collisions = 0;
    private final int[] array;
    private int elements = 0;

    public HashTabell2(int size) {
        this.tableSize = size;
        this.array = new int[size];
    }

    public void insert(int n) {
        int pos = firstHash(n);
        if (array[pos] == 0) {
            array[pos] = n;
            elements++;
        } else {
            int jump = secondHash(n);
            int newPos = probe(pos, jump);
            for (;;){
                collisions++;
                if (array[newPos] == 0) {
                    elements++;
                    array[newPos] = n;
                    break;
                }
                newPos = probe(newPos, jump);
            }
        }
    }

    private int firstHash (int n) {
        return n % tableSize;
    }

    private int secondHash (int n) {
        return (n % (tableSize - 1)) + 1;
    }

    private int probe(int pos, int jump) {
        return (pos + jump) % tableSize;
    }

    public int getAmountOfCollisions() {
        return collisions;
    }

    public double getAmountOfElements() {
        return elements;
    }

    public double getLoadFactor() {
        return getAmountOfElements()/(double)this.tableSize;
    }

    public double getAvgCollisions() {
        return getAmountOfCollisions()/ getAmountOfElements();
    }

    public static void main(String[] args) {
        int tableLength = 12_500_003;
        int length = 10_000_000;
        long start;
        long totTime;
        HashTabell2 hashTabell = new HashTabell2(tableLength);
        int[] array = new int[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int)(Math.random()* length * 10);
        }

        start = System.nanoTime();
        for (int i = 0; i < length; i++) {
            hashTabell.insert(array[i]);
        }
        totTime = System.nanoTime() - start;

        System.out.println("Time to insert " + length + " elements in self made HashTable with size " + tableLength + ": " + totTime/1000000 + " ms");

        HashMap<Integer, Integer> javaHash = new HashMap<>(length);
        start = System.nanoTime();
        for (int i = 0; i < length; i++) {
            javaHash.put(i, array[i]);
        }
        totTime = System.nanoTime() - start;
        System.out.println("Total amount of collisions: " + hashTabell.getAmountOfCollisions());
        System.out.println("Load factor: " + hashTabell.getLoadFactor());
        System.out.println("Avg collisions per element: " + hashTabell.getAvgCollisions());
        System.out.println();
        System.out.println("Time to insert " + length + " elements in Java's HashMap: " + totTime/1000000 + " ms");
    }

}
